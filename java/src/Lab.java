import java.math.BigInteger;
import java.util.Random;
import java.util.Scanner;
import java.util.concurrent.*;

import static java.lang.String.format;

public class Lab {
    private static int n;
    private static int k;
    private static BigInteger multiply;

    public static void main(String[] args) throws InterruptedException {
        Scanner sc = new Scanner(System.in);
        n = sc.nextInt();
        k = sc.nextInt();

        ExecutorService threadPool = Executors.newFixedThreadPool(6);
        multiply = new BigInteger("1");

        long start = System.nanoTime();

        for (int i = 0; i < k; i++) {
            threadPool.execute(Lab::calc);
        }

        threadPool.shutdown();
        threadPool.awaitTermination(90, TimeUnit.SECONDS);

        System.out.println(multiply);
        System.out.println(format("Executed by %d s, value : %d",
                (System.nanoTime() - start) / (1000_000_000),
                multiply.toString(10).length()));
    }

    static void calc() {
        long sum = 0;
        Random rand = new Random();
        for (int i = 0; i < n; i++) {
            int val = rand.nextInt(10);
            sum += val * val * val;
        }
        synchronized (multiply) {
            multiply = multiply.multiply(
                    new BigInteger(String.valueOf(sum))
            );
        }
    }
}
