#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <time.h>

#include <tommath.h>

#include "thpool.h"

// умножение
mp_int mul;
// мьютекс
pthread_mutex_t mutex;

void calc(int*);

int main(int argc, char* argv[]) {
    if (argc < 3) {
        puts("Wrong number of arguments");
        exit(1);
    }

    // инициализация ебучего числа
    mp_init(&mul);
    // зануляем умножение
    mp_set_int(&mul, 1);

	puts("Making threadpool with 6 threads");
	threadpool thpool = thpool_init(6);

    int k = atoi(argv[1]);
    int n = atoi(argv[2]);

    struct timespec start, end;
    clock_gettime(CLOCK_MONOTONIC, &start);

	for (int i = 0; i < k; i++) {
        thpool_add_work(thpool, &calc, &n);
	};

    thpool_wait(thpool);

	puts("Killing threadpool");
	thpool_destroy(thpool);
    clock_gettime(CLOCK_MONOTONIC, &end);
    double elapsed = (end.tv_sec - start.tv_sec);
    elapsed += (end.tv_nsec - start.tv_nsec) / 1000000000.0;

    printf("Time taken %f seconds\n", elapsed);

    printf("Bits %d\n", mp_count_bits(&mul));

    mp_clear(&mul);
	
	return 0;
}

void calc(int* n) {
    unsigned int seed = time(NULL);
    unsigned long sum = 0;
    for (int i = 0; i < *n; i++) {
        // unsigned int value = rand_r(&seed) % 50;
        sum += i;
    }
    // локаем мьютекс
    pthread_mutex_lock(&mutex);
    mp_int mp_sum;
    mp_init(&mp_sum);
    mp_set_int(&mp_sum, sum);
    mp_mul(&mul, &mp_sum, &mul);
    pthread_mutex_unlock(&mutex);
}
