extern crate num_cpus;
extern crate num_bigint;
extern crate num_traits;
extern crate threadpool;
extern crate rand;

pub mod serial;
pub mod threading;
