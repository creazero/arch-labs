extern crate rand;
extern crate num_bigint;
extern crate num_traits;
extern crate threadpool;
extern crate num_cpus;
extern crate rayon;

/// Последовательное выполнение
#[allow(dead_code)]
mod serial;
/// Поточное выполнение
#[allow(dead_code)]
mod threading;
/// Параллельное выполнение
#[allow(dead_code)]
mod parallel;

mod utils;

use std::env;
use std::error::Error;
use std::process::exit;
use std::time::Instant;

use utils::{RunType, UnknownRunTypeError, print_result};

fn main() {
    let args: Vec<String> = env::args().collect();

    if args.len() != 4 {
        usage(0);
    }

    let (run_type, k, n) = match parse_args(args) {
        Ok(tuple) => tuple,
        Err(err) => {
            eprintln!("{}", err);
            usage(1);
        }
    };

    match run_type {
        RunType::Serial => {
            println!("Timer starts...");
            // запускаем таймер
            let now = Instant::now();
            let result = serial::calculate(k, n);
            let elapsed = now.elapsed();
            print_result(result, elapsed);
        },
        RunType::Threading => {
            println!("Timer starts...");
            // запускаем таймер
            let now = Instant::now();
            let result = threading::calculate(k, n);
            let elapsed = now.elapsed();
            print_result(result, elapsed);
        },
        RunType::Parallel => {
            println!("Timer starts...");
            // запускаем таймер
            let now = Instant::now();
            let result = parallel::calculate(k, n);
            let elapsed = now.elapsed();
            print_result(result, elapsed);
        },
    }
}

fn usage(code: i32) -> ! {
    eprintln!("usage: lab [TYPE] [ROWS] [COLS]\n\
               Where TYPE is 'threading', 'parallel' or 'serial'");
    exit(code);
}

/// Парсим аргументы командной строки
fn parse_args(args: Vec<String>) -> Result<(RunType, usize, usize), Box<Error>> {
    let run_type = determine_runtype(&args[1])?;

    let rows: usize = args[2].parse()?;
    let cols: usize = args[3].parse()?;

    Ok((run_type, rows, cols))
}

/// Определяем тип запускаемой задачи
fn determine_runtype(arg: &String) -> Result<RunType, UnknownRunTypeError> {
    match arg.as_ref() {
        "threading" => Ok(RunType::Threading),
        "serial" => Ok(RunType::Serial),
        "parallel" => Ok(RunType::Parallel),
        _ => Err(UnknownRunTypeError)
    }
}
