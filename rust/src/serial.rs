use num_bigint::BigInt;
use num_traits::One;
use rand::prelude::*;

/// Подсчет без внешней матрицы
pub fn calculate(k: usize, n: usize) -> BigInt {
    let mut multiply = One::one();
    let mut rng = thread_rng();
    for _ in 0..k {
        let mut sum = 0;
        for _ in 0..n {
            let value: u32 = rng.gen_range(1, 10);
            sum += value.pow(3);
        }
        multiply *= sum;
    }

    multiply
}
