use std::fmt;
use std::error::Error;
use std::time::Duration;

use num_bigint::BigInt;

pub enum RunType {
    Serial,
    Threading,
    Parallel
}

#[derive(Debug)]
pub struct UnknownRunTypeError;

impl Error for UnknownRunTypeError {
    fn description(&self) -> &str {
        "TYPE should be one of 'threading', 'serial' or 'parallel'!"
    }

    fn cause(&self) -> Option<&Error> { None }
}

impl fmt::Display for UnknownRunTypeError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "TYPE should be one of 'threading', 'serial' or 'parallel'!")
    }
}

/// Пишем про результат
pub fn print_result(result: BigInt, elapsed: Duration) {
    println!("Time elapsed: {:?}", elapsed);
    let mut result_str: String = result.to_str_radix(10);
    let result_str_len = result_str.len();
    if result_str.len() > 100 {
        result_str = substring(&result_str, 0, 100);
        result_str.push_str("...");
    }
    println!("Result is {}\nLength is {}", result_str, result_str_len);
}

/// Получение подстроки
fn substring(str: &String, start: usize, len: usize) -> String {
    str.chars().skip(start).take(len).collect()
}
