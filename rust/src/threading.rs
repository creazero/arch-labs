use std::sync::mpsc;

use num_cpus;
use num_bigint::BigInt;
use num_traits::One;
use rand::prelude::*;

use threadpool::ThreadPool;

/// Подсчет по формуле с помощью потоков внешней матрицы
pub fn calculate(k: usize, n: usize) -> BigInt {
    let mut multiply: BigInt = One::one();
    // пул потоков
    let pool = ThreadPool::new(num_cpus::get());
    // канал для общения потоков
    let (tx, rx) = mpsc::channel();
    for _ in 0..k {
        let tx = tx.clone();
        // запуск потока в пуле
        pool.execute(move || {
            let mut rng = thread_rng();
            let mut sum = 0;
            for _ in 0..n {
                let value: u32 = rng.gen_range(1, 10);
                sum += value.pow(3);
            }
            // отправление результата в канал
            tx.send(sum).expect("Ошибка при отпр.");
        });
    }
    for _ in 0..k {
        let sum = rx.recv().expect("Ошибка при пол.");
        multiply *= sum;
    }
    // возвращаем результат
    multiply
}

