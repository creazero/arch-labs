use num_bigint::BigInt;
use num_traits::One;
use rand::prelude::*;
use rayon::prelude::*;

pub fn calculate(k: usize, n: usize) -> BigInt {
    // используем параллельные итераторы от rayon
    // и собираем массив сумм
    let sums: Vec<u32> = (0..k)
        .into_par_iter()
        .map(|_: usize| {
            let mut sum = 0;
            let mut rng = thread_rng();
            for _ in 0..n {
                let value: u32 = rng.gen_range(1, 10);
                sum += value.pow(3);
            }
            sum
        }).collect();
    // возвращаем произведение сумм
    sums
        .iter()
        .fold(One::one(), |acc, x| acc * x)
}

